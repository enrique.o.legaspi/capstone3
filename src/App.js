import React, {useState} from 'react';
import './App.css';
import {Container, Row, Col} from 'react-bootstrap';
import ErrorPage from './components/ErrorPage';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Login from './pages/Login';
import PageNotFound from './pages/PageError'
import SpecificProduct from './pages/SpecificProduct';

import { UserProvider } from './UserContext';

// For routes
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';

function App() {

  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={ <Home /> }/>
            <Route path="/products" element={ <Products /> }/>
            <Route path="/register" element={ <Register /> }/>
            <Route path="/login" element={ <Login /> }/>
            <Route path="/logout" element={ <Logout /> }/>
            <Route path="/products/:productId" element={<SpecificProduct />} />
            <Route path="*" element={ <ErrorPage /> }/>
          </Routes>
        </Container>
      </Router>  
    </UserProvider>
  );
}

export default App;
