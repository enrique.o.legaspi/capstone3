import React, {useState, useContext} from 'react';
import {Navbar, Nav} from 'react-bootstrap'
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {

	const { user } = useContext(UserContext)

	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);

	return (

			<Navbar bg="dark" expand="lg" variant="dark">
				<Navbar.Brand href="#home" className="ms-5 ps-2">Keta's Kicks & Grind</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav"/>
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ms-auto">
						<Nav.Link as={Link} to="/">Home</Nav.Link>
						<Nav.Link as={Link} to="/products">Products</Nav.Link>							

						{ (user.accessToken !== null) ?
							<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
							:
							<>
								<Nav.Link as={Link} to="/register">Register</Nav.Link>
								<Nav.Link as={Link} to="/login">Login</Nav.Link>
							</>
						}


					</Nav>
				</Navbar.Collapse>
			</Navbar>
		)

}
