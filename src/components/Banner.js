import React from 'react';
import {Row, Col, Button} from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';


export default function Banner() {
	return(

			<Row>
				<Col className="p-5">
					<h1 className="mb-3">Wear your Best. Stay on Spirit.</h1>
					<p className="my-3">Best Shoes, For Best Style.</p>
					<Button variant="primary" border="">Check our Products!</Button>
				</Col>
			</Row>

		)
}