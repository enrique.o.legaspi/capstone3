import React from 'react';
import {Row, Col, Card} from 'react-bootstrap';


export default function Highlights() {
	return(
			<Row>
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>AIR JORDAN 1</h2>
							</Card.Title>
							<Card.Text>
								Celebrating the shoe that started it all, here's a look at the lineage of Michael Jordan's first signature shoe — the Air Jordan 1. Designed by Peter Moore, the Jordan 1 originally released from 1985 to 1986. Quite a few colorways of the model were produced, as well as a few different variations. Following its initial run in the 80s, the Jordan 1 jumpstarted the retro era in 1994, following Jordan's retirement from basketball. Browse our collection of the one and only "GOAT", AIR JORDAN.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>ADIDAS ORIGINALS</h2>
							</Card.Title>
							<Card.Text>
								Originally made for basketball courts in the '70s. Celebrated by hip hop royalty in the '80s. The adidas Superstar shoe is now a lifestyle staple for streetwear enthusiasts. The world-famous shell toe feature remains, providing style and protection. Just like it did on the B-ball courts back in the day. Keep your style and shoes on point. Upgrade your wardrobe with the freshest colorways and latest looks that are available exclusively at KETA'S KICKS AND GRIND.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>NIKE SB STEFAN JANOSKI</h2>
							</Card.Title>
							<Card.Text>
								One of the shoes under the Nike SB line was the signature skateboarding shoe of American professional skateboarder Stefan Janoski that was released in 2009. The Nike SB Stefan Janoski represents Nike's commitment to being part of the skateboarding community. Our collection of the baddest SB shoes would surely pump up your grinding style. Check availability now.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		)

}