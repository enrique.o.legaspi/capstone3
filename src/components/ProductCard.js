import {Row, Col, Card, Button} from 'react-bootstrap'
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

	const { _id, productName, description, price} = productProp;

	// const [count, setCount] = useState(0);
	// const [seats, setSeats] = useState(30);

	// //statehook that indicates availability of course for enrollment(enroll)
	// const [isOpen, setIsOpen] = useState(true)

	// console.log(count);


	// const enroll = () => {
	// 		setCount(count + 1);
	// 		console.log("Enrollees: " + count);
	// 		setSeats(seats - 1)
	// 		console.log("Seats: " + seats)
	// }

	// //When you call useEffect, you're telling React to run you "effect" function after flushing changes to the DOM. Effects are declared inside the component so they have access to its props and states
	// useEffect(() => {
	// 	if(seats === 0) {
	// 		setIsOpen(false);
	// 	}
	// }, [seats])//it controls the rendering of the useEffect


	return(

			<Card className="m-3">
				<Card.Body>
					<Card.Title>{productName}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>	
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>
					
					<Link className="btn btn-primary" to={`/products/${_id}`} >View Product</Link>

					
				</Card.Body>
			</Card>

		)
}


//Check if the CourseCard component is getting the correct prop types
//PropTypes are used for validationg information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next
ProductCard.propTypes = {
	//shape() method it is used to check if a prop object conforms to a specific 'shape'
	productProp: PropTypes.shape({
		//Define the properties and their expected types
		productName: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

