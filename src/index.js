import React from 'react';
import ReactDOM from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';

// const name = 'John Smith';
// let job = "Professional Driver";

// const personDisplay1 = (
//   <>
//       <h1>Hello, {name}</h1>
//       <h3>{job}</h3>
//   </>
//   )

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
      <App />
    
);

// JSX it is a syntax used in Reactjs
//Javascript + XML, It is an extension of JS that let's us create objects which will then be compiled and added as HTML elements

//With JSX, we are able to create HTML elements using JS

//index.js is the main entry point. It is importing App, binding it to root in index.html
