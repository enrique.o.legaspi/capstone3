const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores, et, culpa. Commodi, amet, delectus, dolorum ad deserunt, nam numquam veritatis saepe tempore esse dolor. Veniam doloribus harum in laboriosam accusamus.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores, et, culpa. Commodi, amet, delectus, dolorum ad deserunt, nam numquam veritatis saepe tempore esse dolor. Veniam doloribus harum in laboriosam accusamus.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores, et, culpa. Commodi, amet, delectus, dolorum ad deserunt, nam numquam veritatis saepe tempore esse dolor. Veniam doloribus harum in laboriosam accusamus.",
		price: 55000,
		onOffer: true
	}
]

export default coursesData;