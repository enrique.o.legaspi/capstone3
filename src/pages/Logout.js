import React, { useContext, useEffect } from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout() {
	
	const { unsetUser, setUser } = useContext(UserContext);
	unsetUser();

	useEffect(() => {
		//set the user state back to its original value = empty
		setUser({ accessToken: null})
	}, [])

	return(

		<Navigate to="/" />

		)
}