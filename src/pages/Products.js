import React, { useContext, useState, useEffect } from 'react';
// import coursesData from '../mockData/coursesData';
import ProductCard from '../components/ProductCard';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';

import UserContext from '../UserContext';

export default function Products() {
	
	const { user } = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);

	const fetchData = () => {
		fetch('http://localhost:4000/products/all')
		.then(res => res.json())
		.then(data => {
			console.log(data)
			
			setAllProducts(data);
		})
	}

	useEffect(() => {
		fetchData()

	}, [])

	return (
			// <>
			// 	<h1>Courses</h1>
			// 	{courses}
			// </>

			<>
				{(user.isAdmin === true) ?
					<AdminView productsData={allProducts} fetchData={fetchData} />

					:

					<UserView productsData={allProducts} />

				}
			</>
				

		)
}