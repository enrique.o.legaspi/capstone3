import React, { useState, useEffect , useContext} from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';

export default function Register() {

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	//State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	//State to determine whether submit button is enabled or not for conditional rendering
	const [isActive, setIsActive] = useState(true);


	useEffect(() => {
		//Validation to enable submit button when all fields are populated and both passwords match
		if((firstName !=='' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, password1, password2])


	function registerUser(e) {
		//Prevents page redirection via form submission
		e.preventDefault();

		fetch('http://localhost:4000/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if(data){
				
				Swal.fire({
					title:'Success!',
					icon: 'success',
					text: 'You have been registered. Enter Credentials to Login.'
				})

				navigate('/login')

			}else {
				Swal.fire({
					title: 'Ooops!',
					icon: 'error',
					text: 'Something Went Wrong. Check your Credentials.'
				})
			}

			setFirstName('');
			setLastName('');
			setEmail('');
			setPassword('');

		})

	}

	return (

			(user.accessToken !== null) ?

			<Navigate to="/" />

			:

			<Form onSubmit={(e) => registerUser(e)}>
				<h1>Register</h1>
				<Form.Group> 
					<Form.Label>First Name</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Juan"
						required
						value={firstName}
						onChange={e => setFirstName(e.target.value)}
						/>
					<Form.Label>Last Name</Form.Label>
					<Form.Control 
						type="text"
						placeholder="dela Cruz"
						required
						value={lastName}
						onChange={e => setLastName(e.target.value)}
						/>
				</Form.Group>
				<Form.Group> 
					<Form.Label>Email Address</Form.Label>
					<Form.Control 
						type="email"
						placeholder="user@email.com"
						required
						value={email}
						onChange={e => setEmail(e.target.value)}
						/>
						<Form.Text className="text-muted">
							We'll never share your email with anyone else
						</Form.Text>
				</Form.Group>

				<Form.Group> 
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Enter Password"
						required
						value={password1}
						onChange={e => setPassword1(e.target.value)}
						/>
				</Form.Group>

				<Form.Group> 
					<Form.Label>Verify Password</Form.Label>
					<Form.Control 
						type="password"
						placeholder="Verify Password"
						required
						value={password2}
						onChange={e => setPassword2(e.target.value)}
						/>
				</Form.Group>
				{isActive ? 
					<Button variant="primary" type="submit" className="mt-3">Submit</Button>

					:

					<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>

				}
				
			</Form>

		)
}